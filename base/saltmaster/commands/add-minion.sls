# Add minion on roste
roster line 1:
  cmd.run:
    - name: echo '{{ salt['pillar.get']('TY') }}_{{ salt['pillar.get']('ST') }}_{{ salt['pillar.get']('CO') }}_{{ salt['pillar.get']('PR') }}_{{ salt['pillar.get']('ID') }}:' >> {{ salt['pillar.get']('envs:rosterfile') }}
roster line 2:
  cmd.run:
    - name: echo '   'host':' {{ salt['pillar.get']('IP') }} >> {{ salt['pillar.get']('envs:rosterfile') }}
roster line 3:
  cmd.run:
    - name: echo '   'user':' {{ salt['pillar.get']('envs:user') }} >> {{ salt['pillar.get']('envs:rosterfile') }}
roster line 4:
  cmd.run:
    - name: echo '   'priv':' {{ salt['pillar.get']('envs:key') }} >> {{ salt['pillar.get']('envs:rosterfile') }}
roster line 5:
  cmd.run:
    - name: echo '   'sudo':' True >> {{ salt['pillar.get']('envs:rosterfile') }}
roster line 6:
  cmd.run:
    - name: echo '########################################################' >> {{ salt['pillar.get']('envs:rosterfile') }}
roster line 7:
  cmd.run:
    - name: echo " " >> {{ salt['pillar.get']('envs:rosterfile') }}

# Add the minion on known_hosts
add known hosts:
  cmd.run:
    - name: {{ salt['pillar.get']('envs:sshcopyid') }} {{ salt['pillar.get']('envs:key') }} {{ salt['pillar.get']('envs:user') }}@{{ salt['pillar.get']('IP') }}

# append minion_id on saltmaster/pillar/minionlist
append new minion_id:
  cmd.run:
    - name: echo '  '{{ salt['pillar.get']('TY') }}_{{ salt['pillar.get']('ST') }}_{{ salt['pillar.get']('CO') }}_{{ salt['pillar.get']('PR') }}_{{ salt['pillar.get']('ID') }}':' {{ salt['pillar.get']('TY') }}_{{ salt['pillar.get']('ST') }}_{{ salt['pillar.get']('CO') }}_{{ salt['pillar.get']('PR') }}_{{ salt['pillar.get']('ID') }} >> /srv/master.saltstack/pillars/base/saltmaster/minionslist/init.sls

# update pillar with new minion_id
update pillar:
  cmd.run:
    - name: salt SaltMaster saltutil.refresh_pillar

# Create grains for the minion
grain add line 1:
  cmd.run:
    - name: echo type':' {{ salt['pillar.get']('TY') }} > {{ salt['pillar.get']('TY') }}_{{ salt['pillar.get']('ST') }}_{{ salt['pillar.get']('CO') }}_{{ salt['pillar.get']('PR') }}_{{ salt['pillar.get']('ID') }}.grain
grain add line 2:
  cmd.run:
    - name: echo country':' {{ salt['pillar.get']('CO') }} >> {{ salt['pillar.get']('TY') }}_{{ salt['pillar.get']('ST') }}_{{ salt['pillar.get']('CO') }}_{{ salt['pillar.get']('PR') }}_{{ salt['pillar.get']('ID') }}.grain
grain add line 3:
  cmd.run:
    - name: echo state':' {{ salt['pillar.get']('ST') }} >> {{ salt['pillar.get']('TY') }}_{{ salt['pillar.get']('ST') }}_{{ salt['pillar.get']('CO') }}_{{ salt['pillar.get']('PR') }}_{{ salt['pillar.get']('ID') }}.grain
grain add line 4:
  cmd.run:
    - name: echo provider':' {{ salt['pillar.get']('PR') }} >> {{ salt['pillar.get']('TY') }}_{{ salt['pillar.get']('ST') }}_{{ salt['pillar.get']('CO') }}_{{ salt['pillar.get']('PR') }}_{{ salt['pillar.get']('ID') }}.grain

# scp minion grain to minion
scp grain:
  cmd.run:
    - name: scp -i {{ salt['pillar.get']('envs:key') }} {{ salt['pillar.get']('TY') }}_{{ salt['pillar.get']('ST') }}_{{ salt['pillar.get']('CO') }}_{{ salt['pillar.get']('PR') }}_{{ salt['pillar.get']('ID') }}.grain {{ salt['pillar.get']('envs:user') }}@{{ salt['pillar.get']('IP') }}:{{ salt['pillar.get']('envs:sgrains') }}

# Install the minion packages

# add apt key on minion
exec apt key add:
  cmd.run:
  - name: salt-ssh -r {{ salt['pillar.get']('TY') }}_{{ salt['pillar.get']('ST') }}_{{ salt['pillar.get']('CO') }}_{{ salt['pillar.get']('PR') }}_{{ salt['pillar.get']('ID') }} "sudo wget -O - https://repo.saltstack.com/apt/ubuntu/16.04/amd64/latest/SALTSTACK-GPG-KEY.pub | sudo apt-key add -"

scp salt list:
  cmd.run:
    - name: scp -i {{ salt['pillar.get']('envs:key') }} {{ salt['pillar.get']('envs:saltaptrepofile') }} {{ salt['pillar.get']('envs:user') }}@{{ salt['pillar.get']('IP') }}:{{ salt['pillar.get']('envs:ssaltaptrepofile') }}

mv salt list:
  cmd.run:
    - name: salt-ssh -r {{ salt['pillar.get']('TY') }}_{{ salt['pillar.get']('ST') }}_{{ salt['pillar.get']('CO') }}_{{ salt['pillar.get']('PR') }}_{{ salt['pillar.get']('ID') }} "sudo mv /home/ubuntu/saltstack.list /etc/apt/sources.list.d/saltstak.list"

exec apt update:
  cmd.run:
    - name: salt-ssh -r {{ salt['pillar.get']('TY') }}_{{ salt['pillar.get']('ST') }}_{{ salt['pillar.get']('CO') }}_{{ salt['pillar.get']('PR') }}_{{ salt['pillar.get']('ID') }} "sudo apt-get update"

install minion:
  cmd.run:
    - name: salt-ssh -r {{ salt['pillar.get']('TY') }}_{{ salt['pillar.get']('ST') }}_{{ salt['pillar.get']('CO') }}_{{ salt['pillar.get']('PR') }}_{{ salt['pillar.get']('ID') }} "sudo apt-get install salt-minion salt-common -y"

#move grain file
move grain:
  cmd.run:
    - name: salt-ssh -r {{ salt['pillar.get']('TY') }}_{{ salt['pillar.get']('ST') }}_{{ salt['pillar.get']('CO') }}_{{ salt['pillar.get']('PR') }}_{{ salt['pillar.get']('ID') }} "sudo mv /home/ubuntu/grains /etc/salt/grains"

# Create a file with the name of the minion that's gonna be used on minion instalation
# and to accept the minion on Master
#
create minion id:
  cmd.run:
    - name: echo {{ salt['pillar.get']('TY') }}_{{ salt['pillar.get']('ST') }}_{{ salt['pillar.get']('CO') }}_{{ salt['pillar.get']('PR') }}_{{ salt['pillar.get']('ID') }} > {{ salt['pillar.get']('envs:dirbase') }}{{ salt['pillar.get']('TY') }}_{{ salt['pillar.get']('ST') }}_{{ salt['pillar.get']('CO') }}_{{ salt['pillar.get']('PR') }}_{{ salt['pillar.get']('ID') }}.name

# Copy via SSH the minion name and the configuration file to minion connect on master

scp minion conf:
  cmd.run:
    - name: scp -i {{ salt['pillar.get']('envs:key') }} {{ salt['pillar.get']('envs:saltminionconf') }} {{ salt['pillar.get']('envs:user') }}@{{ salt['pillar.get']('IP') }}:{{ salt['pillar.get']('envs:ssaltminionconf') }}

scp minion id:
  cmd.run:
    - name: scp -i {{ salt['pillar.get']('envs:key') }} {{ salt['pillar.get']('envs:dirbase') }}{{ salt['pillar.get']('TY') }}_{{ salt['pillar.get']('ST') }}_{{ salt['pillar.get']('CO') }}_{{ salt['pillar.get']('PR') }}_{{ salt['pillar.get']('ID') }}.name {{ salt['pillar.get']('envs:user') }}@{{ salt['pillar.get']('IP') }}:{{ salt['pillar.get']('envs:sminionid') }}

mv minion conf:
  cmd.run:
    - name: salt-ssh -r {{ salt['pillar.get']('TY') }}_{{ salt['pillar.get']('ST') }}_{{ salt['pillar.get']('CO') }}_{{ salt['pillar.get']('PR') }}_{{ salt['pillar.get']('ID') }} "sudo mv /home/ubuntu/minion /etc/salt/minion"

mv minion id:
  cmd.run:
    - name: salt-ssh -r {{ salt['pillar.get']('TY') }}_{{ salt['pillar.get']('ST') }}_{{ salt['pillar.get']('CO') }}_{{ salt['pillar.get']('PR') }}_{{ salt['pillar.get']('ID') }} "sudo mv /home/ubuntu/minion_id /etc/salt/minion_id"

sleep time 1:
  cmd.run:
    - name: sleep 5


# Run the minion agent
#
start minion:
  cmd.run:
    - name: salt-ssh -r {{ salt['pillar.get']('TY') }}_{{ salt['pillar.get']('ST') }}_{{ salt['pillar.get']('CO') }}_{{ salt['pillar.get']('PR') }}_{{ salt['pillar.get']('ID') }} "sudo salt-minion -d"

sleep time 2:
  cmd.run:
    - name: sleep 5

accept newminion:
  cmd.run:
    - name: salt-key -a {{ salt['pillar.get']('TY') }}_{{ salt['pillar.get']('ST') }}_{{ salt['pillar.get']('CO') }}_{{ salt['pillar.get']('PR') }}_{{ salt['pillar.get']('ID') }} -y

sleep time 3:
  cmd.run:
    - name: sleep 5

# sync new grains
sync grains:
  cmd.run:
    - name: salt {{ salt['pillar.get']('TY') }}_{{ salt['pillar.get']('ST') }}_{{ salt['pillar.get']('CO') }}_{{ salt['pillar.get']('PR') }}_{{ salt['pillar.get']('ID') }} saltutil.sync_grains
