mkdiretcipb:
  cmd.run:
    - name: mkdir /etc/ipb

echogitrepo:
  cmd.run:
    - name: git clone https://{{ salt['pillar.get']('gitrepobase:gituser') }}:{{ salt['pillar.get']('gitrepobase:gitpass') }}@{{ salt['pillar.get']('gitrepobase:gitrepo') }} /etc/ipb

installfirehol:
  cmd.run:
    - name: bash /etc/ipb/scripts/apply -s vpn
