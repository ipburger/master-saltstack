rm-current-config:
  cmd.run:
    - name: rm /etc/ssh/sshd_config

sshd_config_create:
  file.managed:
    - name: /etc/ssh/sshd_config
    - user: sshd
    - group: ssh
    - mode: 664

sshd_config_populate:
  file.append:
    - name: /etc/ssh/sshd_config
    - text:
      - {% for data, conf in pillar.get('confs', {}).iteritems() %}
      - {{conf}}{% endfor %}

