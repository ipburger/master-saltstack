base:
#
#GRAINS
#
  'type:TEST':
    - test
#
  'type:DNS'
    - dns
#
  'type:JKS'
    - jks
#
  'type:MW'
    - mw
#
  'type:SSH'
    - ssh-server
#
  'type:VPN'
    - vpn
#
#NOMECLATURES
#
  'SaltMaster':
    - saltmaster
#
  "TEST_*_*_*_*"
    - test
#
  '*':
    - common
#
  "DNS_*_*_*_*":
    - dns
#
  "JKS_*_*_*_*":
    - jenkis
#
  "MW_*_*_*_*":
    - mw
#
  "SSH_*_*_*_*":
    - ssh-server
#
  "VPN_*_*_*_*":
    - vpn
