users:
  saltpad:
    fullname: Saltpad
    uid: 1010
    shell: /bin/bash
    password: $6$MdDmVtlx$sxvEdblgSrmpQL1pxY10Qi5qFoqqdVHSNJW7sSjmg8Ce4vnpMP23FcYNQqTrMTxA.jqqw137dpzJGi5BnoOA2/
    groups:
      - wheel
      - sudo
    ssh_auth:
      - ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDDFl8yNlMf1WjF/3YIu6+GnVZtZm3xdEZj4O4aVkiIi1/wxuUyuoGCxKzvLroVYJx6h/df97uvpd0ZzfS0Ce8wt+JeV5q8MehW5Y/BT0Wz1vwin9/AOlg943Vtz3pzb3Q2k+jMzJHvtV0EPeghUNRnWDguz7lwT2C8kX8JE7DBsh41zMgG7V/jOsl4PuQqpBJaro14X8AMDrFnvgMWzhXayihRtSmch60KlfYXeDtv0c5PX9I0NShbcQf7+UNPb2t7izTIhZga2CEEblPZERpFzI/pplHhE9cKCtVc95QcXvSJVFMV+reA6p1i9HH9u4wT/Y+sAD0aYAHgAsZi4KAD saltpad@rudder
    user_files:
      - enabled: true
