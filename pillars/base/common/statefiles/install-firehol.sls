os-update:
  cmd.run:
    - name: apt-get -qq update
install-requeriments:
  cmd.run:
    - name: if [[ $(dpkg -l | grep "ii  ipset" | wc -l) || $(dpkg -l | grep "ii  iprange" | wc -l) || $(dpkg -l | grep "ii  nfacct" | wc -l) || $(dpkg -l | grep "ii  traceroute" | wc -l) -ne 1 ]]; then apt-get -qq install ipset iprange nfacct traceroute -y; fi
download-deb-files:
  cmd.run:
    - name: mkdir -p /tmp/firehol; if [[ $(ls -l /tmp/firehol | grep firehol | wc -l) -ne 2 ]]; then wget https://launchpad.net/ubuntu/+archive/primary/+files/firehol_3.1.5+ds-1ubuntu1_all.deb -P /tmp/firehol/ && wget https://launchpad.net/ubuntu/+archive/primary/+files/firehol-common_3.1.5+ds-1ubuntu1_all.deb -P /tmp/firehol; fi
install-packages:
  cmd.run:
    - name: dpkg -i /tmp/firehol/f*
rm-tmp:
  cmd.run:
    - name: rm -rf /tmp/firehol
