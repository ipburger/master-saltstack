base:
#
# NOMECLATURES
#
  'TEST_QEB_CA_OVH_1':
    - bind
  'SaltMaster':
#    - saltmaster
    - saltmaster.environments
    - saltmaster.minionslist
  '*':
#    - common
    - common.users
#    - common.conf
#    - common.ssh_public_keys
  "*_*_DNS_*":
#    - dns
#    - dns.users
#    - dns.conf
#    - dns.ssh_public_keys
  "*_*_JKS_*":
    - jenkis
    - jenkis.users
    - jenkis.conf
    - jenkis.ssh_public_keys
  "*_*_MW_*":  
    - mw
    - mw.users
    - mw.conf
    - mw.ssh_public_keys
  "*_*_SSH_*":
#    - ssh-server
#    - ssh-server.users
    - ssh-server.conf
#    - ssh-server.ssh_public_keys
  "*_*_VPN_*":
#    - vpn
#    - vpn.users
    - vpn.conf
#    - vpn.ssh_public_keys
#
# Grains
#
  'type:DNS':
    - dns
    - dns.users
    - dns.conf
    - dns.ssh_public_keys
  'type:JKS':
    - jks
    - jks.users
    - jks.conf
    - jks.ssh_public_keys
  'type:mw':
    - mw
    - mw.users
    - mw.conf
    - mw.ssh_public_keys
  'type:SSH':
#    - ssh-server
#    - ssh-server.users
    - ssh-server.conf
#    - ssh-server.ssh_public_keys
  'type:VPN':
#    - vpn
#    - vpn.users
    - vpn.conf
#    - vpn.ssh_public_keys
